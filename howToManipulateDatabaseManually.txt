###SE CONNECTER A LA BASE DE DONNEE HEROKU
etty@slave-node:~/Documents/messe-django-v1$ heroku pg:psql
 ›   Warning: heroku update available from 7.43.1 to 7.43.2.
--> Connecting to postgresql-vertical-71678
psql (12.4 (Ubuntu 12.4-1.pgdg18.04+1))
Connexion SSL (protocole : TLSv1.2, chiffrement : ECDHE-RSA-AES256-GCM-SHA384, bits : 256, compression : désactivé)
Saisissez « help » pour l'aide.

##POUR UNE INSTALLATION
'Il faut se référrer au fichier "initial_db_SQL_(postgresql).sql" dans le dossier initialConfig'




###POUR LA MANIPULATION DE LA DATABASE DEJA CREEE
damp-sierra-52116::DATABASE=> delete from messeBack_clientuser where id=1;
ERROR:  relation "messeback_clientuser" does not exist
LIGNE 1 : delete from messeBack_clientuser where id=1;
                      ^
damp-sierra-52116::DATABASE=> \dt
                     Liste des relations
 Schéma |            Nom             | Type  |  Propriétaire  
--------+----------------------------+-------+----------------
 public | auth_group                 | table | eplzazrqsappvp
 public | auth_group_permissions     | table | eplzazrqsappvp
 public | auth_permission            | table | eplzazrqsappvp
 public | auth_user                  | table | eplzazrqsappvp
 public | auth_user_groups           | table | eplzazrqsappvp
 public | auth_user_user_permissions | table | eplzazrqsappvp
 public | django_admin_log           | table | eplzazrqsappvp
 public | django_content_type        | table | eplzazrqsappvp
 public | django_migrations          | table | eplzazrqsappvp
 public | django_session             | table | eplzazrqsappvp
 public | messeBack_archidiocese     | table | eplzazrqsappvp
 public | messeBack_clientuser       | table | eplzazrqsappvp
 public | messeBack_creneaureligieux | table | eplzazrqsappvp
 public | messeBack_diocese          | table | eplzazrqsappvp
 public | messeBack_eglise           | table | eplzazrqsappvp
 public | messeBack_eglise_creneaux  | table | eplzazrqsappvp
 public | messeBack_event            | table | eplzazrqsappvp
 public | messeBack_event_pretres    | table | eplzazrqsappvp
 public | messeBack_pretre           | table | eplzazrqsappvp
 public | messeBack_seeked           | table | eplzazrqsappvp
(20 lignes)

damp-sierra-52116::DATABASE=> select * from messeBack_clientuser;
ERROR:  relation "messeback_clientuser" does not exist
LIGNE 1 : select * from messeBack_clientuser;
                        ^
damp-sierra-52116::DATABASE=> select * from public.messeBack_clientuser;
ERROR:  relation "public.messeback_clientuser" does not exist
LIGNE 1 : select * from public.messeBack_clientuser;
                        ^
damp-sierra-52116::DATABASE=> select * from public."messeBack_clientuser";
 id | username | firstname | lastname |    password     |    type    |          time          | eglise_id 
----+----------+-----------+----------+-----------------+------------+------------------------+-----------
  1 | jojo     | Jordan    | Adopo    | adopojordan1996 | Superadmin | 2020-09-23 17:24:24+00 |         1
(1 ligne)

damp-sierra-52116::DATABASE=> delete from public."messeBack_clientuser" where id=1;
DELETE 1
damp-sierra-52116::DATABASE=> ALTER TABLE table_name 
damp-sierra-52116::DATABASE-> RENAME COLUMN column_name TO new_c^Cumn_name
damp-sierra-52116::DATABASE=> ALTER public."messeBack_clientuser" RENAME COLUMN time to tokentime;
ERROR:  syntax error at or near "public"
LIGNE 1 : ALTER public."messeBack_clientuser" RENAME COLUMN time to to...
                ^
damp-sierra-52116::DATABASE=> ALTER TABLE public."messeBack_clientuser" RENAME COLUMN time to tokentime;
ALTER TABLE
damp-sierra-52116::DATABASE=> ALTER TABLE public."messeBack_clientuser" SET COLUMN tokentime TIMESTAMP DEFAULT NOW();
ERROR:  syntax error at or near "COLUMN"
LIGNE 1 : ALTER TABLE public."messeBack_clientuser" SET COLUMN tokenti...
                                                        ^
damp-sierra-52116::DATABASE=> ALTER TABLE public."messeBack_clientuser" UPDATE COLUMN tokentime TIMESTAMP DEFAULT NOW();
ERROR:  syntax error at or near "UPDATE"
LIGNE 1 : ALTER TABLE public."messeBack_clientuser" UPDATE COLUMN toke...
                                                    ^
damp-sierra-52116::DATABASE=> ALTER TABLE public."messeBack_clientuser" DELETE COLUMN tokentime;
ERROR:  syntax error at or near "DELETE"
LIGNE 1 : ALTER TABLE public."messeBack_clientuser" DELETE COLUMN toke...
                                                    ^
damp-sierra-52116::DATABASE=> ALTER TABLE public."messeBack_clientuser" ALTER COLUMN tokentime TYPE TIMESTAMP WITH TIME ZONE USING tokentime AT TIME ZONE 'UTC' DEFAULT NOW();
ERROR:  syntax error at or near "DEFAULT"
LIGNE 1 : ...WITH TIME ZONE USING tokentime AT TIME ZONE 'UTC' DEFAULT NO...
                                                               ^
damp-sierra-52116::DATABASE=> ALTER TABLE public."messeBack_clientuser" ALTER COLUMN tokentime TYPE TIMESTAMP WITH TIME ZONE USING tokentime AT TIME ZONE 'UTC';
ALTER TABLE
damp-sierra-52116::DATABASE=> ALTER TABLE public."messeBack_clientuser" ALTER COLUMN tokentime SET DEFAULT NOW();
ALTER TABLE
damp-sierra-52116::DATABASE=> select * from public."messeBack_clientuser";
 id | username | firstname | lastname | password | type | tokentime | eglise_id 
----+----------+-----------+----------+----------+------+-----------+-----------
(0 ligne)

damp-sierra-52116::DATABASE=> ALTER TABLE public."messeBack_clientuser" ADD COLUMN token VARCHAR;
ALTER TABLE
damp-sierra-52116::DATABASE=> ALTER TABLE public."messeBack_clientuser" ALTER COLUMN token TYPE VARCHAR(200);
ALTER TABLE
damp-sierra-52116::DATABASE=> ALTER TABLE public."messeBack_clientuser" ALTER COLUMN token DROP NOT NULL;
ALTER TABLE
damp-sierra-52116::DATABASE=> 
