    
    function getCookie(name)
    {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?

                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }


    function highlightInputs(){
        $("#form-username").animate({ backgroundColor:"#FFDAB9"}, 'slow');
        $("#form-password").animate({ backgroundColor:"#FFDAB9"}, 'slow');
        setTimeout(function(){
            $("#form-username").animate({backgroundColor:"initial"}, 'slow');
            $("#form-password").animate({backgroundColor:"initial"}, 'slow');
        }, 3000);
    }

    function slideToggleTextResponse() {
        $('#text-reponse').slideToggle();
        setTimeout(function(){
            $('#text-reponse').slideToggle();
        }, 3000);
    }

    $('#text-reponse').slideToggle();
    $(".login-btn").bind('click', function(){
        let username=$("#form-username").val();
        let password=$("#form-password").val();
        console.log("Utilisateur: "+username)
        console.log("CSRFToken: "+token)
        let response="";
        $.post("https://damp-sierra-52116.herokuapp.com/login/",
            // headers: { "X-CSRFToken": token, "Access-Control-Allow-Origin":"*" },
            // crossDomain: true,
            // url: "https://damp-sierra-52116.herokuapp.com/messe/login",
            // method: 'POST', // or another (GET), whatever you need
            // // dataType: 'json',
            // data: {
            {
                username: username, // data you need to pass to your function
                password: password,
                click: true
            },
            // success: 
            function (data, status) {
                console.log("Status: "+ status);
                console.log(data);
                console.log("data: "+JSON.stringify(data));

            
                if(status=="success"){
                var resp = JSON.parse(JSON.stringify(data));
                var htmlObject = resp["content"];
                console.log(resp["data"]);
                response=htmlObject;
                // alert(response);
                // $("#result").html("<h3>"+response+"</h3>");
                // $('#exampleModal').modal('show');
                // $('.modal-body').html("<h4>"+response+"</h4>");
                document.cookie = "user="+resp["data"]+"; expires="+new Date(new Date().getTime() + 24 * 60 * 60 * 1000)+"; path=/";
                console.log(document.cookie);
                location.href="https://damp-sierra-52116.herokuapp.com/dash/"
                }else if (JSON.parse(JSON.stringify(data))['status']=="404") {
                    var resp = JSON.parse(JSON.stringify(data));
                    var htmlObject = resp["content"];
                console.log("Login erreur");
                //     var resp = JSON.parse(JSON.stringify(e));
                // var htmlObject = $(resp["responseText"]);
                // $('#loginModal').modal('show');
                $('#text-reponse').text("le nom d'utilisateur ou le mot de passe ne correspondent pas!");
                // $(".modal-footer").hide();
                // $("#result").html(htmlObject);
                // console.log('User not found :'+JSON.stringify(e));
                }
            }).fail(function(jqXHR, textStatus, errorThrown){
                
                highlightInputs();
                slideToggleTextResponse();
                if(jqXHR.status == 404) {
                    $('#loginModal').modal('show');
                    $('.modal-body').html("le nom d'utilisateur ou le mot de passe ne correspondent pas! Veuillez réessayer...");
                    $(".modal-footer").hide();
                }else{
                    $('#loginModal').modal('show');
                    $('.modal-body').html("Une erreur est survenue lors de votre connexion, veuillez réessayer!");
                    $(".modal-footer").hide();
                }
            });
            // ,
            // error:function (e) {        
            //     var resp = JSON.parse(JSON.stringify(e));
            //     var htmlObject = $(resp["responseText"]);
            //     $('#exampleModal').modal('show');
            //     $('.modal-body').html(htmlObject);;
            //     // $("#result").html(htmlObject);
            //     console.log('User not found :'+JSON.stringify(e));
            // }
        });

    $("#gotodash").bind('click', function() {
        location.href="https://damp-sierra-52116.herokuapp.com/dash/"
    });
    

    var elem=document.getElementById("new_pass_form");
    if(typeof elem !== 'undefined' && elem !== null) {
        elem.style.display = "none";
      }

    $("#forgotpass").bind('click', function() {
        // if(document.getElementById("new_pass_form").style.display == "block"){
        //     document.getElementById("new_pass_form").style.display = "none";
        // }else{
            document.getElementById("new_pass_form").style.display = "block";
        // };
    });
    $(".change-btn").bind('click', function() {
        let email=$("#email_pass").val();
        console.log("Email: "+email);
        let response="";
        location.href="https://damp-sierra-52116.herokuapp.com/user/0/?action=changepassword&email="+email;
            
    });
        // return false;
    // });

    document.addEventListener("keydown", function(event) {
        if (event.key === "Enter") {
            event.preventDefault();
            let username=$("#form-username").val();
        let password=$("#form-password").val();
        console.log("Utilisateur: "+username)
        console.log("CSRFToken: "+token)
        let response="";
        $.post("https://damp-sierra-52116.herokuapp.com/login/",
            // headers: { "X-CSRFToken": token, "Access-Control-Allow-Origin":"*" },
            // crossDomain: true,
            // url: "https://damp-sierra-52116.herokuapp.com/messe/login",
            // method: 'POST', // or another (GET), whatever you need
            // // dataType: 'json',
            // data: {
            {
                username: username, // data you need to pass to your function
                password: password,
                click: true
            },
            // success: 
            function (data, status) {
                console.log("Status: "+ status);
                console.log(data);
                console.log("data: "+JSON.stringify(data));

            
                if(status=="success"){
                var resp = JSON.parse(JSON.stringify(data));
                var htmlObject = resp["content"];
                console.log(resp["data"]);
                response=htmlObject;
                // alert(response);
                // $("#result").html("<h3>"+response+"</h3>");
                // $('#exampleModal').modal('show');
                // $('.modal-body').html("<h4>"+response+"</h4>");
                document.cookie = "user="+resp["data"]+"; expires="+new Date(new Date().getTime() + 24 * 60 * 60 * 1000)+"; path=/";
                console.log(document.cookie);
                location.href="https://damp-sierra-52116.herokuapp.com/dash/"
                }else if (JSON.parse(JSON.stringify(data))['status']=="404") {
                    var resp = JSON.parse(JSON.stringify(data));
                    var htmlObject = resp["content"];
                console.log("Login erreur");
                //     var resp = JSON.parse(JSON.stringify(e));
                // var htmlObject = $(resp["responseText"]);
                // $('#loginModal').modal('show');
                $('#text-reponse').text("le nom d'utilisateur ou le mot de passe ne correspondent pas!");
                // $(".modal-footer").hide();
                // $("#result").html(htmlObject);
                // console.log('User not found :'+JSON.stringify(e));
                }
            }).fail(function(jqXHR, textStatus, errorThrown){
                
                highlightInputs();
                slideToggleTextResponse();
                if(jqXHR.status == 404) {
                    $('#loginModal').modal('show');
                    $('.modal-body').html("le nom d'utilisateur ou le mot de passe ne correspondent pas! Veuillez réessayer...");
                    $(".modal-footer").hide();
                }else{
                    $('#loginModal').modal('show');
                    $('.modal-body').html("Une erreur est survenue lors de votre connexion, veuillez réessayer!");
                    $(".modal-footer").hide();
                }
            });

        }
    });

