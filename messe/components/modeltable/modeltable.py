from django_components import component
from django.http import Http404, HttpResponse, HttpResponseServerError, JsonResponse, HttpResponseRedirect
from django.conf import settings
from django.middleware import csrf

class Modeltable(component.Component):
    def context(self, tablename):
        
        return {
            "tablename":tablename
            }

    def template(self, context):
        return "modeltable/modeltable.html"

    class Media:
        css = {'all': ['modeltable/modeltable.css']}
        js = ['modeltable/modeltable.js']

# component.registry.register(name="login", component=Login)
