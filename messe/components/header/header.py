from django_components import component
from django.http import Http404, HttpResponse, HttpResponseServerError, JsonResponse, HttpResponseRedirect
from django.conf import settings
from django.middleware import csrf

class Header(component.Component):
    def context(self, eglisename, username, lastname, firstname, usertype):
        
        return {
            "eglisename":eglisename, 
            "username": username,
            "lastname": username,
            "firstname": firstname,
            "usertype": usertype
            }

    def template(self, context):
        return "header/header.html"

    class Media:
        css = {'all': ['header/header.css']}
        js = ['header/header.js']

# component.registry.register(name="login", component=Login)
