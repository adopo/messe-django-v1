from django_components import component
from django.http import Http404, HttpResponse, HttpResponseServerError, JsonResponse, HttpResponseRedirect
from django.conf import settings
from django.middleware import csrf

class Login(component.Component):
    def context(self):
        
        return {"appName":settings.APP_NAME, "csrf_token":csrf._get_new_csrf_token()}

    def template(self, context):
        return "login/login.html"

    class Media:
        css = {'all': ['login/login.css']}
        js = ['login/login.js']

# component.registry.register(name="login", component=Login)
