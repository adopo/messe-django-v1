"""messe URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import include, path
from messeBack import views
urlpatterns = [
    url(r'^messe/', include('messeBack.urls')),
    url(r'^admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('login/', views.doLogin, name='login'),
    path('dash/', views.dashboard, name='dash'),
    path('updatetable/', views.update_table, name='update_table'),

    path('eglise/<int:eglise_id>/', views.eglise_form, name='eglise_form'),
    path('diocese/<int:diocese_id>/', views.diocese_form, name='diocese_form'),
    path('archidiocese/<int:archidiocese_id>/', views.archidiocese_form, name='archidiocese_form'),
    path('event/<int:event_id>/', views.event_form, name='event_form'),
    path('creneau/<int:creneau_id>/', views.creaneau_form, name='creneau_form'),
    path('clientuser/<int:user_id>/', views.user_form, name='user_form'),
    path('demande/<int:demande_id>/', views.demande_form, name='demande_form'),

    path('newpassword/', views.new_password, name='new_password_form'),

    path('egliseaction/', views.save_eglise, name='eglise_action'),
    path('dioceseaction/', views.save_diocese, name='diocese_action'),
    path('eventaction/', views.save_event, name='event_action'),
    path('archidioceseaction/', views.save_archi, name='archi_action'),
    path('useraction/', views.save_user, name='user_action'),
    path('creneauaction/', views.save_creneau, name='creneau_action'),
    path('demandeaction/', views.save_demande, name='demande_action'),


    path('events/', views.get_events, name='load_events')

]
