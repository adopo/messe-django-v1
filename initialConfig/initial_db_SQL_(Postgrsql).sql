--
-- Create model Archidiocese
--
CREATE TABLE "messeBack_archidiocese" ("id" serial NOT NULL PRIMARY KEY, "denomination" varchar(200) NOT NULL);
--
-- Create model CreneauReligieux
--
CREATE TABLE "messeBack_creneaureligieux" ("id" serial NOT NULL PRIMARY KEY, "jour" varchar(200) NOT NULL, "heure" time NOT NULL);
--
-- Create model Diocese
--
CREATE TABLE "messeBack_diocese" ("id" serial NOT NULL PRIMARY KEY, "denomination" varchar(200) NOT NULL, "Communes" varchar(100)[100] NOT NULL, "archidiocese_id" integer NOT NULL);
--
-- Create model Eglise
--
CREATE TABLE "messeBack_eglise" ("id" serial NOT NULL PRIMARY KEY, "denomination" varchar(200) NOT NULL, "addresse" varchar(200) NOT NULL, "type" varchar(200) NOT NULL, "commune" varchar(200) NOT NULL, "tel" varchar(10) NOT NULL, "cel" varchar(10) NOT NULL, "email" varchar(200) NOT NULL, "web" varchar(200) NOT NULL, "facebook" varchar(200) NOT NULL, "youtube" varchar(200) NOT NULL, "latitude" double precision NOT NULL, "longitude" double precision NOT NULL, "diocese_id" integer NOT NULL);
CREATE TABLE "messeBack_eglise_creneaux" ("id" serial NOT NULL PRIMARY KEY, "eglise_id" integer NOT NULL, "creneaureligieux_id" integer NOT NULL);
--
-- Create model Seeked
--
CREATE TABLE "messeBack_seeked" ("id" serial NOT NULL PRIMARY KEY, "value" varchar(200) NOT NULL, "iteration" integer NOT NULL);
--
-- Create model Pretre
--
CREATE TABLE "messeBack_pretre" ("id" serial NOT NULL PRIMARY KEY, "nom" varchar(200) NOT NULL, "prenoms" varchar(200) NOT NULL, "titre" varchar(200) NOT NULL, "eglise_id" integer NOT NULL);
--
-- Create model Event
--
CREATE TABLE "messeBack_event" ("id" serial NOT NULL PRIMARY KEY, "denomination" varchar(200) NOT NULL, "type" varchar(200) NOT NULL, "time" timestamp with time zone NOT NULL, "eglise_id" integer NOT NULL);
CREATE TABLE "messeBack_event_pretres" ("id" serial NOT NULL PRIMARY KEY, "event_id" integer NOT NULL, "pretre_id" integer NOT NULL);
--
-- Create model DemandeMesse
--
CREATE TABLE "messeBack_demandemesse" ("id" serial NOT NULL PRIMARY KEY, "name" varchar(200) NOT NULL, "objet" varchar(200) NOT NULL, "description" text NOT NULL, "event_id" integer NOT NULL);
--
-- Create model ClientUser
--
CREATE TABLE "messeBack_clientuser" ("id" serial NOT NULL PRIMARY KEY, "username" varchar(200) NOT NULL, "firstname" varchar(200) NOT NULL, "lastname" varchar(200) NOT NULL, "password" varchar(200) NOT NULL, "email" varchar(200) NOT NULL, "type" varchar(200) NOT NULL, "token" varchar(200) NOT NULL, "tokentime" varchar(200) NOT NULL, "eglise_id" integer NOT NULL);
ALTER TABLE "messeBack_diocese" ADD CONSTRAINT "messeBack_diocese_archidiocese_id_40a1f5fc_fk_messeBack" FOREIGN KEY ("archidiocese_id") REFERENCES "messeBack_archidiocese" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX "messeBack_diocese_archidiocese_id_40a1f5fc" ON "messeBack_diocese" ("archidiocese_id");
ALTER TABLE "messeBack_eglise" ADD CONSTRAINT "messeBack_eglise_diocese_id_1b8f1893_fk_messeBack_diocese_id" FOREIGN KEY ("diocese_id") REFERENCES "messeBack_diocese" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX "messeBack_eglise_diocese_id_1b8f1893" ON "messeBack_eglise" ("diocese_id");
ALTER TABLE "messeBack_eglise_creneaux" ADD CONSTRAINT "messeBack_eglise_creneau_eglise_id_creneaureligie_7efd37e7_uniq" UNIQUE ("eglise_id", "creneaureligieux_id");
ALTER TABLE "messeBack_eglise_creneaux" ADD CONSTRAINT "messeBack_eglise_cre_eglise_id_1a518659_fk_messeBack" FOREIGN KEY ("eglise_id") REFERENCES "messeBack_eglise" ("id") DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "messeBack_eglise_creneaux" ADD CONSTRAINT "messeBack_eglise_cre_creneaureligieux_id_dfd07b1f_fk_messeBack" FOREIGN KEY ("creneaureligieux_id") REFERENCES "messeBack_creneaureligieux" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX "messeBack_eglise_creneaux_eglise_id_1a518659" ON "messeBack_eglise_creneaux" ("eglise_id");
CREATE INDEX "messeBack_eglise_creneaux_creneaureligieux_id_dfd07b1f" ON "messeBack_eglise_creneaux" ("creneaureligieux_id");
ALTER TABLE "messeBack_pretre" ADD CONSTRAINT "messeBack_pretre_eglise_id_12ad7dd3_fk_messeBack_eglise_id" FOREIGN KEY ("eglise_id") REFERENCES "messeBack_eglise" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX "messeBack_pretre_eglise_id_12ad7dd3" ON "messeBack_pretre" ("eglise_id");
ALTER TABLE "messeBack_event" ADD CONSTRAINT "messeBack_event_eglise_id_9988a913_fk_messeBack_eglise_id" FOREIGN KEY ("eglise_id") REFERENCES "messeBack_eglise" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX "messeBack_event_eglise_id_9988a913" ON "messeBack_event" ("eglise_id");
ALTER TABLE "messeBack_event_pretres" ADD CONSTRAINT "messeBack_event_pretres_event_id_pretre_id_2adabb87_uniq" UNIQUE ("event_id", "pretre_id");
ALTER TABLE "messeBack_event_pretres" ADD CONSTRAINT "messeBack_event_pretres_event_id_c688665a_fk_messeBack_event_id" FOREIGN KEY ("event_id") REFERENCES "messeBack_event" ("id") DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "messeBack_event_pretres" ADD CONSTRAINT "messeBack_event_pret_pretre_id_b51bc5b2_fk_messeBack" FOREIGN KEY ("pretre_id") REFERENCES "messeBack_pretre" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX "messeBack_event_pretres_event_id_c688665a" ON "messeBack_event_pretres" ("event_id");
CREATE INDEX "messeBack_event_pretres_pretre_id_b51bc5b2" ON "messeBack_event_pretres" ("pretre_id");
ALTER TABLE "messeBack_demandemesse" ADD CONSTRAINT "messeBack_demandemesse_event_id_6e85d245_fk_messeBack_event_id" FOREIGN KEY ("event_id") REFERENCES "messeBack_event" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX "messeBack_demandemesse_event_id_6e85d245" ON "messeBack_demandemesse" ("event_id");
ALTER TABLE "messeBack_clientuser" ADD CONSTRAINT "messeBack_clientuser_eglise_id_6dbd1a3a_fk_messeBack_eglise_id" FOREIGN KEY ("eglise_id") REFERENCES "messeBack_eglise" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX "messeBack_clientuser_eglise_id_6dbd1a3a" ON "messeBack_clientuser" ("eglise_id");
COMMIT;
