from django.urls import path

from . import views

urlpatterns = [
    path('index/', views.index, name='index'),
    path('program/', views.programmes, name='program'),
    path('detail/<int:eglise_id>/', views.detail, name='detail'),
    path('search/<str:name>', views.filter_by_name, name='search'),
    path('search/', views.filter_by_name, name='search'),
    path('proximity', views.events_proximity, name='proximity'),
    path('login', views.doLogin, name='login'),
    path('dash', views.dashboard, name='dash'),
]
