# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django_components import component
from django.shortcuts import render, redirect, get_object_or_404
from django.core import serializers
from django.http import Http404, HttpResponse, HttpResponseServerError, JsonResponse, HttpResponseRedirect
from messeBack.models import Archidiocese, Diocese, Eglise, Event, Pretre, Seeked, ClientUser, CreneauReligieux, DemandeMesse
import json
import datetime
import time
from django.db.models import Q
from decimal import Decimal
import requests
from django.conf import settings
from messe.components.login.login import Login
from messe.components.header.header import Header
from messe.components.modeltable.modeltable import Modeltable
from django.middleware import csrf
from django.views.decorators.csrf import csrf_protect, csrf_exempt, CsrfViewMiddleware
from django.utils import timezone

#INDEX VIEW
def index(request):
    if request.COOKIES.get('user') is not None:
        cookie = request.COOKIES.get('user')
        cookie_dict=json.loads(cookie)
        tokentime=cookie_dict['tokentime']
        print(tokentime)
        datetime_object = time.strptime(tokentime, "%d/%m/%Y, %H:%M:%S")
        if time.strptime(datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S"),"%d/%m/%Y, %H:%M:%S")<datetime_object:
            return redirect('/dash')

    context={"appName":settings.APP_NAME, "csrf_token":csrf._get_new_csrf_token()}
    all_components = component.registry.all()
    if "login" not in all_components:
        component.registry.register(name="login", component=Login)
    return render(request,"index.html",context=context)
    # HttpResponse("Akwaba! You're at the Messe App index.")


#MOBILE ENDPOINTS

# function that returns all program from now
def programmes(request):
    """now = datetime.datetime.now()
    program = Event.objects.filter(time__gte=now).order_by('-starttime')[:5]
    data = ', '.join(["{'name': \""+q.denomination+"\", 'type': \""+q.get_type_display()+"\", 'eglise': {'id':"+str(q.eglise.id)+", 'denomination':\""+q.eglise.denomination+"\", 'addresse':\""+q.eglise.addresse+"\", 'commune':\""+q.eglise.commune+"\", 'category':\""+q.eglise.get_type_display()+"\"}, 'diocese': \""+q.eglise.diocese.denomination+"\", 'pretres':["+", ".join(["{'lastname':\""+p.nom+"\", 'firstname':\""+p.prenoms+"\"}" for p in q.pretres.all()])+"], 'latitude':"+q.eglise.latitude+", 'longitude':"+q.eglise.longitude+",'time': \""+q.get_time()+"\"}" for q in program])
    output="["+data+"]"
    output=json.dumps(output, ensure_ascii=False)"""
    return HttpResponse(get_all_data(wChurchs=True))
    # return HttpResponse("Vous aurez d'ici peu de temps les résultats des évènements réligieux catholiques")

#function that makes search filters
def filter_by_name(request, name=None):
    now = datetime.datetime.now()
    program = Event.objects.filter(starttime__gte=now).order_by('-starttime')[:5]
    result=[]

    for E in program:
        if name:
            if name.lower() in E.denomination.lower():
                result.append(E)
                check_search(E.denomination)

            elif str(name) in str(E.time):
                result.append(E)
                check_search(str(E.time))

            elif name.lower() in E.eglise.denomination.lower():
                result.append(E)
                check_search(E.eglise.denomination)

            elif name.lower() in E.get_type_display().lower():
                result.append(E)
                check_search(E.get_type_display())

            elif name.lower() in E.eglise.commune.lower():
                result.append(E)
                check_search(E.eglise.commune)

            elif name.lower()=="simple":
                name="messe"
                if name in E.get_type_display().lower():
                    result.append(E)        
            elif name.lower() in E.eglise.diocese.denomination.lower():
                result.append(E)
                check_search(E.eglise.diocese.denomination)

            else:
                for n in E.pretres.all():
                    print(name.lower() in n.nom.lower() or (name.lower() in n.prenoms.lower()))
                    if (name.lower() in n.nom.lower()):
                        result.append(E)
                        check_search(n.nom)

                    elif (name.lower() in n.prenoms.lower()):
                        result.append(E)
                        check_search(n.prenoms)

                if len(result) == 0:
                    raise Http404("Votre recherche `%s` n'existe pas"%name)
        else:
            date=request.GET.get('q', None)
            print(str(date))
            if str(date) is not None:
                #format 1 mm/dd/yyyy
                if str(date) in E.time.strftime("%m/%d/%Y, %H:%M:%S"):
                    result.append(E)
                elif str(date) in E.time.strftime("%m-%d-%Y, %H:%M:%S"):
                    result.append(E)
                elif str(date) in E.time.strftime("%m-%d-%Y %H:%M:%S"):
                    result.append(E)
                elif str(date) in E.time.strftime("%m/%d/%Y %H:%M:%S"):
                    result.append(E)
                #format 2 dd/mm/yyyy
                elif str(date) in E.time.strftime("%d/%m/%Y, %H:%M:%S"):
                    result.append(E)
                elif str(date) in E.time.strftime("%d-%m-%Y, %H:%M:%S"):
                    result.append(E)
                elif str(date) in E.time.strftime("%d-%m-%Y %H:%M:%S"):
                    result.append(E)
                elif str(date) in E.time.strftime("%d/%m/%Y %H:%M:%S"):
                    result.append(E)
                #format 3 yyyy/mm/dd
                elif str(date) in E.time.strftime("%Y/%m/%d, %H:%M:%S"):
                    result.append(E)
                elif str(date) in E.time.strftime("%Y-%m-%d, %H:%M:%S"):
                    result.append(E)
                elif str(date) in E.time.strftime("%Y-%m-%d %H:%M:%S"):
                    result.append(E)
                elif str(date) in E.time.strftime("%Y/%m/%d %H:%M:%S"):
                    result.append(E)
                #format 4 yyyy/dd/mm
                elif str(date) in E.time.strftime("%Y/%d/%m, %H:%M:%S"):
                    result.append(E)
                elif str(date) in E.time.strftime("%Y-%d-%m, %H:%M:%S"):
                    result.append(E)
                elif str(date) in E.time.strftime("%Y-%d-%m %H:%M:%S"):
                    result.append(E)
                elif str(date) in E.time.strftime("%Y/%d/%m %H:%M:%S"):
                    result.append(E)
            else:
                raise Http404("Votre requete `%s` n'existe pas"%date)
    
        
    program_by_name = ', '.join(["{'name': \""+q.denomination+"\", 'type': \""+q.get_type_display()+"\", 'eglise': {'id':"+str(q.eglise.id)+", 'denomination':\""+q.eglise.denomination+"\", 'addresse':\""+q.eglise.addresse+"\", 'commune':\""+q.eglise.commune+"\", 'category':\""+q.eglise.get_type_display()+"\"}, 'diocese': \""+q.eglise.diocese.denomination+"\", 'pretres':["+", ".join(["{'lastname':\""+p.nom+"\", 'firstname':\""+p.prenoms+"\"}" for p in q.pretres.all()])+"], 'latitude':"+str(q.eglise.latitude)+", 'longitude':"+str(q.eglise.longitude)+",'time': \""+q.get_time()+"\"}" for q in result])

    # response = "Vous aurez bientôt les résultats pour %s."
    # return HttpResponse(response % church_name)

    output="{\"events\": ["+program_by_name+"], \"eglises\":["+', '.join("{\"denomination\": "+k.denomination+",\"latitude\":"+str(k.latitude)+", \"longitude\":"+str(k.longitude)+"}" for k in Eglise.objects.all())+"]}"
    # output=json.dumps(output, ensure_ascii=False)
    return HttpResponse(json.loads(output))

# function that return ddetail on eglise
def detail(request, eglise_id):
    try:
        church =  Eglise.objects.get(pk=eglise_id)
        diocese = serializers.serialize("json",Diocese.objects.filter(denomination=church.diocese.denomination))
        pretres = serializers.serialize("json",Pretre.objects.filter(eglise__id=eglise_id))
        now = datetime.datetime.now()
        events = Event.objects.filter(Q(starttime__gte=now)&Q(eglise__id=eglise_id)).order_by('-starttime')[:5]
        program_by_name = ', '.join(["{'name': \""+q.denomination+"\", 'type': \""+q.get_type_display()+"\", 'eglise': {'id':"+str(q.eglise.id)+", 'denomination':\""+q.eglise.denomination+"\", 'addresse':\""+q.eglise.addresse+"\", 'commune':\""+q.eglise.commune+"\", 'category':\""+q.eglise.get_type_display()+"\"}, 'diocese': \""+q.eglise.diocese.denomination+"\", 'pretres':["+", ".join(["{'lastname':\""+p.nom+"\", 'firstname':\""+p.prenoms+"\"}" for p in q.pretres.all()])+"], 'latitude':"+str(q.eglise.latitude)+", 'longitude':"+str(q.eglise.longitude)+",'time': \""+q.get_time()+"\"}" for q in events])

        # response = "Vous aurez bientôt les résultats pour %s."
        # return HttpResponse(response % church_name)
        output="["+program_by_name+"]"
        events=json.dumps(output, ensure_ascii=False)

        creneaux=serializers.serialize("json",church.creneaux.all())
        print()
        result={'nom':church.denomination, 'diocese':array_from_search(diocese), 'creneaux':array_from_search(creneaux), 'pretres':array_from_search(pretres), 'tel':church.tel, 'cel':church.cel, 'email':church.email, 'facebook':church.facebook, 'web':church.web, 'youtube':church.youtube, 'events':json.loads(events)}
        return JsonResponse(result, safe=False, json_dumps_params={'ensure_ascii': False})
    except Exception as e:
        raise Http404(e)
        
#function that filters clearly fields on a objects list of a model
def array_from_search(search):
    prs=[]
    for k in json.loads(search, encoding='utf-8'):
        prs.append(k['fields'])
    return prs

#view action that returns all events near a user according to his position
def events_proximity(request):
    orlat=request.GET.get('lat', 0.0)
    orlng=request.GET.get('lng', 0.0)
    #api-key maps
    API_KEY=settings.MAPS_API_KEY
    print(API_KEY)
      
    return HttpResponse(get_all_data())

#function that returns all the programm since now
def get_all_data(wChurchs=False):
    if wChurchs:
        now = datetime.datetime.now()
        program = Event.objects.filter(starttime__gte=now).order_by('-starttime')[:5]
        data = ', '.join(["{\"name\": \""+q.denomination+"\", \"type\": \""+q.get_type_display()+"\", \"eglise\": {\"id\":"+str(q.eglise.id)+", \"denomination\":\""+q.eglise.denomination+"\", \"addresse\":\""+q.eglise.addresse+"\", \"commune\":\""+q.eglise.commune+"\", \"category\":\""+q.eglise.get_type_display()+"\"}, \"diocese\": \""+q.eglise.diocese.denomination+"\", \"pretres\":["+", ".join(["{\"lastname\":\""+p.nom+"\", \"firstname\":\""+p.prenoms+"\"}" for p in q.pretres.all()])+"], \"latitude\":"+str(q.eglise.latitude)+", \"longitude\":"+str(q.eglise.longitude)+",\"time\": \""+q.get_time()+"\"}" for q in program])
        output="{\"events\": ["+data+"], \"eglises\":["+', '.join("{\"denomination\": "+k.denomination+",\"latitude\":"+str(k.latitude)+", \"longitude\":"+str(k.longitude)+"}" for k in Eglise.objects.all())+"]}"
        output=json.dumps(output, ensure_ascii=False)
    else:
        now = datetime.datetime.now()
        program = Event.objects.filter(starttime__gte=now).order_by('-starttime')[:5]
        data = ', '.join(["{\"name\": \""+q.denomination+"\", \"type\": \""+q.get_type_display()+"\", \"eglise\": {\"id\":"+str(q.eglise.id)+", \"denomination\":\""+q.eglise.denomination+"\", \"addresse\":\""+q.eglise.addresse+"\", \"commune\":\""+q.eglise.commune+"\", \"category\":\""+q.eglise.get_type_display()+"\"}, \"diocese\": \""+q.eglise.diocese.denomination+"\", \"pretres\":["+", ".join(["{\"lastname\":\""+p.nom+"\", \"firstname\":\""+p.prenoms+"\"}" for p in q.pretres.all()])+"], \"latitude\":"+str(q.eglise.latitude)+", \"longitude\":"+str(q.eglise.longitude)+",\"time\": \""+q.get_time()+"\"}" for q in program])
        output="["+data+"]"
        output=json.dumps(output, ensure_ascii=False)
    return json.loads(output)

#function that calculates optimal path (deprecated but can be reused)
def optimal_path_time(json):
    duration=""
    distance=""
    print(json)
    optimal_el=json[0]
    value=json[0].duration.value
    for el in json:
        if el.status=="OK" and el.duration.value<value:
            optimal_el=el
    
    return optimal_el

#function that shows post request infos
def pretty_print_POST(req):
    """
    At this point it is completely built and ready
    to be fired; it is "prepared".

    However pay attention at the formatting used in 
    this function because it is programmed to be pretty 
    printed and may differ from the actual request.
    """
    print('{}\n{}\r\n{}\r\n\r\n{}'.format(
        '-----------START-----------',
        req.method + ' ' + req.url,
        '\r\n'.join('{}: {}'.format(k, v) for k, v in req.headers.items()),
        req.body,
    ))

#function that handles search request filters
def check_search(name):
    recherches = Seeked.objects.all()
    seekCount=0
    for recherche in recherches:
        if recherche.value.lower() == name.lower():
            obj = recherche
            obj.iteration=obj.iteration+1
            obj.save()
        else:
            seekCount=seekCount+1

    if seekCount==len(recherches):
        seek = Seeked(value=name)
        seek.save()

##################### ALL DASHBOARDS VIEW AND ACTION ##################
#view action that logs user in
@csrf_exempt
def doLogin(request):

    users=ClientUser.objects.all()
    if request.POST.get('click', False):
        username = request.POST.get('username')
        password = request.POST.get('password')
        print(username+" "+password)
        print(json.dumps(serializers.serialize("json", users)))
        for user in users:
            if user.username==username and user.password==password:
                # user.token=csrf._get_new_csrf_token()
                # user.tokentime=datetime.timedelta(days=1)
                # user.save()
                tokentime=datetime.datetime.now()+timezone.timedelta(days=1)
                ClientUser.objects.filter(username=username, password=password).update(token=csrf.get_token(request),tokentime=tokentime.strftime("%d/%m/%Y, %H:%M:%S"))
                print(username+" -> isAuth"+str(user.isAuthenticated()))
                user_updated=ClientUser.objects.filter(username=username, password=password)
                user_json = serializers.serialize("json", user_updated)
                data = {"user": user_json}
                result={"status":201, "content": "User "+username+" authenticated successfully!", "data":json.dumps(json.loads(user_json)[0]['fields'])}
                response= JsonResponse(result, safe=False, json_dumps_params={'ensure_ascii': False})
                set_cookie(response, "user",json.dumps(data), days_expire=1)
                return response
            elif user.email==username and user.password==password:
                # user.token=csrf._get_new_csrf_token()
                # user.tokentime=datetime.timedelta(days=1)
                # user.save()
                tokentime=datetime.datetime.now()+timezone.timedelta(days=1)
                ClientUser.objects.filter(email=username, password=password).update(token=csrf.get_token(request),tokentime=tokentime.strftime("%d/%m/%Y, %H:%M:%S"))
                print(username+" -> isAuth"+str(user.isAuthenticated()))
                user_updated=ClientUser.objects.filter(email=username, password=password)
                user_json = serializers.serialize("json", user_updated)
                data = {"user": user_json}
                result={"status":201, "content": "User "+username+" authenticated successfully!", "data":json.dumps(json.loads(user_json)[0]['fields'])}
                response= JsonResponse(result, safe=False, json_dumps_params={'ensure_ascii': False})
                set_cookie(response, "user",json.dumps(data), days_expire=1)
                return response
    context = {
        'status': '404', 'reason': 'User not Found'  
    }
    response = JsonResponse(context, safe=False, json_dumps_params={'ensure_ascii': False})
    response.status_code = 404
    return response

#function that sets cookie after user logged in
def set_cookie(response, key, value, days_expire = 7):
  if days_expire is None:
    max_age = 365 * 24 * 60 * 60  #one year
  else:
    max_age = days_expire * 24 * 60 * 60 
  expires = datetime.datetime.strftime(datetime.datetime.utcnow() + datetime.timedelta(seconds=max_age), "%a, %d-%b-%Y %H:%M:%S GMT")
#   response.set_cookie(key, value, max_age=max_age, expires=expires, samesite='Strict', secure=True)
  response.set_cookie(key, value, max_age=max_age, expires=expires, samesite='Lax')

#DASHBOARD VIEW
@csrf_exempt
def dashboard(request):
    if request.COOKIES.get('user') is None:
        return redirect('/')

    cookie = request.COOKIES.get('user')
    cookie_dict=json.loads(cookie)
    tokentime=cookie_dict['tokentime']
    print(tokentime)
    datetime_object = time.strptime(tokentime, "%d/%m/%Y, %H:%M:%S")
    if time.strptime(datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S"),"%d/%m/%Y, %H:%M:%S")>datetime_object:
        return redirect('/')

    eglisename=Eglise.objects.filter(pk=cookie_dict['eglise'])[0].denomination
    usertype=ClientUser.objects.filter(username=cookie_dict['username'])[0].get_type_display().lower()
    if usertype=='superadmin':
        now = datetime.datetime.now()
        all_eglises=Eglise.objects.all()
        all_dm=DemandeMesse.objects.all()
        all_ev=Event.objects.all()
        nb_par=all_eglises.count()
        nb_dm=all_dm.count()
        active_events=Event.objects.filter(endtime__gte=now).order_by('-starttime')[:5]
        nb_dm_active=0
        for ev in active_events:
            nb_dm_active+=DemandeMesse.objects.filter(event=ev).count()
        nb_dm_paid=DemandeMesse.objects.filter(etat=True).count()
        nb_dm_unpaid=DemandeMesse.objects.filter(etat=False).count()
        context={"appName":settings.APP_NAME, "username":cookie_dict['username'], "lastname":cookie_dict['lastname'], "firstname":cookie_dict['firstname'], "usertype":usertype, "eglisename":eglisename,'nb_par':nb_par, 'nb_dm':nb_dm, 'nb_dm_active':nb_dm_active, 'dm_paid':nb_dm_paid*3000, 'dm_unpaid':nb_dm_unpaid*3000, 'key':'accueil'}

    else:
        now = datetime.datetime.now()
        # all_eglises=Eglise.objects.all()
        all_dm=DemandeMesse.objects.all()
        all_ev=Event.objects.filter( eglise=Eglise.objects.filter(denomination=eglisename).first()).order_by('-starttime')[:5]
        # nb_par=all_eglise.count()
        nb_dm=all_dm.count()
        active_events=Event.objects.filter(endtime__gte=now, eglise=Eglise.objects.filter(denomination=eglisename).first()).order_by('-starttime')[:5]
        nb_dm_active=0
        for ev in active_events:
            nb_dm_active+=DemandeMesse.objects.filter(event=ev).count()
        nb_dm_paid=DemandeMesse.objects.filter(etat=True).count()
        nb_dm_unpaid=DemandeMesse.objects.filter(etat=False).count()
        context={"appName":settings.APP_NAME, "username":cookie_dict['username'], "lastname":cookie_dict['lastname'], "firstname":cookie_dict['firstname'], "usertype":usertype, "eglisename":eglisename,'nb_par':1, 'nb_dm':nb_dm, 'nb_dm_active':nb_dm_active, 'dm_paid':nb_dm_paid*3000, 'dm_unpaid':nb_dm_unpaid*3000, 'key':'accueil'}
    all_components = component.registry.all()
    if "header" not in all_components:
        component.registry.register(name="header", component=Header)
    if "modeltable" not in all_components:
        component.registry.register(name="modeltable", component=Modeltable)
    # all_components = component.registry.all()
    # if "login" not in all_components:
    #     component.registry.register(name="login", component=Login)
    return render(request,"dashboard.html",context=context)

#function that gets or generates new token for user
def get_or_create_csrf_token(request):
    token = request.META.get('X-CSRFToken', None)
    if token is None:
        token = csrf._get_new_csrf_key()
        request.META['X-CSRFToken'] = token
    request.META['CSRF_COOKIE_USED'] = True
    return token

#FORMS FOR MODELS DATA

#form that permits to make an action on church/eglise like creation, update  
@csrf_exempt
def eglise_form(request, eglise_id):
    if request.COOKIES.get('user') is None:
        return redirect('/')

    cookie = request.COOKIES.get('user')
    cookie_dict=json.loads(cookie)
    tokentime=cookie_dict['tokentime']
    print(tokentime)
    datetime_object = time.strptime(tokentime, "%d/%m/%Y, %H:%M:%S")
    if time.strptime(datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S"),"%d/%m/%Y, %H:%M:%S")>datetime_object:
        return redirect('/')
    
    if request.GET.get('action', '')=='delete':
        x=Eglise.objects.get(pk=eglise_id).delete()
        #if x>=1:
        return HttpResponse("Eglise "+str(eglise_id)+" deleted successfully")
        #else:
        #    return HttpResponseServerError("Eglise "+str(eglise_id)+" deletion failed")

    else:
        context={}
        if eglise_id!=0:
            eglise=Eglise.objects.get(id=eglise_id)
            context=eglise.__dict__
        
        context['action']=request.GET.get('action', '')
        context['modification']=context['action']=='edit'
        dioceses=[]
        for diocese in Diocese.objects.all():
            dioceses.append(diocese.__dict__)
        context['dioceses']=dioceses


        creneaux=[]
        for creneau in CreneauReligieux.objects.all():
            print(creneau.__str__)
            creneaux.append(creneau.__dict__)
        context['creneaux']=creneaux
        print(creneaux)
        # pk=eglise_id
        # action=request.GET.get('action', '')
        # modification=action=='modification'
        # egliseName=eglise.denomination
        # denomination=egliseName
        # type=eglise.type
        # diocese=eglise.diocese
        # commune=eglise.commune
        # addresse=eglise.addresse
        # lat=eglise
        
        return render(request,"forms/eglise.html", context=context)

#form that permits to make an action on diocese like creation, update  
@csrf_exempt
def diocese_form(request, diocese_id):
    if request.COOKIES.get('user') is None:
        return redirect('/')

    cookie = request.COOKIES.get('user')
    cookie_dict=json.loads(cookie)
    tokentime=cookie_dict['tokentime']
    print(tokentime)
    datetime_object = time.strptime(tokentime, "%d/%m/%Y, %H:%M:%S")
    if time.strptime(datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S"),"%d/%m/%Y, %H:%M:%S")>datetime_object:
        return redirect('/')
    
    if request.GET.get('action', '')=='delete':
        x=Diocese.objects.get(pk=diocese_id).delete()
        #if x>=1:
        return HttpResponse("Diocèse "+str(diocese_id)+" deleted successfully")
        #else:
        #    return HttpResponseServerError("Diocèse "+str(diocese_id)+" deletion failed")

    else:
        context={}
        if diocese_id!=0:
            diocese=Diocese.objects.get(id=diocese_id)
            context=diocese.__dict__
            print(context)
        
        context['action']=request.GET.get('action', '')
        context['modification']=context['action']=='edit'
        archidioceses=[]
        for archidiocese in Archidiocese.objects.all():
            archidioceses.append(archidiocese.__dict__)
        context['archidioceses']=archidioceses
        # pk=eglise_id
        # action=request.GET.get('action', '')
        # modification=action=='modification'
        # egliseName=eglise.denomination
        # denomination=egliseName
        # type=eglise.type
        # diocese=eglise.diocese
        # commune=eglise.commune
        # addresse=eglise.addresse
        # lat=eglise
        
        return render(request,"forms/diocese.html", context=context)

#form that permits to make an action on demande(messe) like creation, update  
@csrf_exempt
def demande_form(request, demande_id):
    if request.COOKIES.get('user') is None:
        return redirect('/')

    cookie = request.COOKIES.get('user')
    cookie_dict=json.loads(cookie)
    tokentime=cookie_dict['tokentime']
    print(tokentime)
    datetime_object = time.strptime(tokentime, "%d/%m/%Y, %H:%M:%S")
    if time.strptime(datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S"),"%d/%m/%Y, %H:%M:%S")>datetime_object:
        return redirect('/')
    
    if request.GET.get('action', '')=='delete':
        x=DemandeMesse.objects.get(pk=demande_id).delete()
        #if x>=1:
        return HttpResponse("Demande "+str(demande_id)+" deleted successfully")
        #else:
        #    return HttpResponseServerError("Demande "+str(demande_id)+" deletion failed")

    else:
        context={}
        if demande_id!=0:
            demande=DemandeMesse.objects.get(id=demande_id)
            context=demande.__dict__
            print(context)
        
        context['action']=request.GET.get('action', '')
        context['modification']=context['action']=='edit'
        messes=[]
        for event in Event.objects.all():
            eglise=event.eglise.denomination
            eventjson=event.__dict__
            eventjson['eglise']=eglise
            eventjson['time']=event.get_time()
            messes.append(eventjson)
        context['messes']=messes
        
        return render(request,"forms/demande.html", context=context)

#form that permits to make an action on archidiocese like creation, update  
@csrf_exempt
def archidiocese_form(request, archidiocese_id):
    if request.COOKIES.get('user') is None:
        return redirect('/')

    cookie = request.COOKIES.get('user')
    cookie_dict=json.loads(cookie)
    tokentime=cookie_dict['tokentime']
    print(tokentime)
    datetime_object = time.strptime(tokentime, "%d/%m/%Y, %H:%M:%S")
    if time.strptime(datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S"),"%d/%m/%Y, %H:%M:%S")>datetime_object:
        return redirect('/')
    
    if request.GET.get('action', '')=='delete':
        x=Archidiocese.objects.get(pk=archidiocese_id).delete()
        #if x>=1:
        return HttpResponse("Archidiocèse "+str(archidiocese_id)+" deleted successfully")
        #else:
        #   return HttpResponseServerError("Archidiocèse "+str(archidiocese_id)+" deletion failed")

    else:
        context={}
        if archidiocese_id!=0:
            archidiocese=Archidiocese.objects.get(id=archidiocese_id)
            context=archidiocese.__dict__
        
        context['action']=request.GET.get('action', '')
        context['modification']=context['action']=='edit'
        
        return render(request,"forms/archidiocese.html", context=context)

#form that permits to make an action on event like creation, update  
@csrf_exempt
def event_form(request, event_id):
    if request.COOKIES.get('user') is None:
        return redirect('/')

    cookie = request.COOKIES.get('user')
    cookie_dict=json.loads(cookie)
    tokentime=cookie_dict['tokentime']
    print(tokentime)
    datetime_object = time.strptime(tokentime, "%d/%m/%Y, %H:%M:%S")
    if time.strptime(datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S"),"%d/%m/%Y, %H:%M:%S")>datetime_object:
        return redirect('/')
    
    if request.GET.get('action', '')=='delete':
        x=Event.objects.get(pk=event_id).delete()
        #if x>=1:
        return HttpResponse("Evènement "+str(event_id)+" deleted successfully")
        #else:
        #    return HttpResponseServerError("Evènement "+str(event_id)+" deletion failed")

    else:
        context={}
        if event_id!=0:
            event=Event.objects.get(id=event_id)
            context=event.__dict__
            print(context)

        
        context['action']=request.GET.get('action', '')
        context['modification']=context['action']=='edit'
        pres=[]
        for pretre in Pretre.objects.all():
            pres.append(pretre.__dict__)
        context['pres']=pres
        print(pres)
        eglises=[]
        for eglise in Eglise.objects.all():
            eglises.append(eglise.__dict__)
        context['eglises']=eglises
        
        return render(request,"forms/event.html", context=context)

#form that permits to make an action on creneau like creation, update  
@csrf_exempt
def creaneau_form(request, creneau_id):
    if request.COOKIES.get('user') is None:
        return redirect('/')

    cookie = request.COOKIES.get('user')
    cookie_dict=json.loads(cookie)
    tokentime=cookie_dict['tokentime']
    print(tokentime)
    datetime_object = time.strptime(tokentime, "%d/%m/%Y, %H:%M:%S")
    if time.strptime(datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S"),"%d/%m/%Y, %H:%M:%S")>datetime_object:
        return redirect('/')
    
    if request.GET.get('action', '')=='delete':
        x=CreneauReligieux.objects.get(pk=creneau_id).delete()
        #if x>=1:
        return HttpResponse("Créneau "+str(creneau_id)+" deleted successfully")
        #else:
        #    return HttpResponseServerError("Créneau "+str(creneau_id)+" deletion failed")

    else:
        context={}
        if creneau_id!=0:
            creneau=CreneauReligieux.objects.get(id=creneau_id)
            context=creneau.__dict__
            print(context)

        
        context['action']=request.GET.get('action', '')
        context['modification']=context['action']=='edit'
        
        
        return render(request,"forms/creneau.html", context=context)

#form that permits to make an action on user like creation, update  
@csrf_exempt
def user_form(request, user_id):

    if request.GET.get('action', '')=='changepassword':
        email=request.GET.get('email', '')
        try:
            user=ClientUser.objects.get(email=email)
            context=user.__dict__
            return render(request,"forms/change_password.html", context=context)
        except ClientUser.DoesNotExist:
            context={"status":"unknow", "email":email}
            return render(request,"forms/change_password.html", context=context)
            #raise Http404("Aucun utilisateur avec cet email("+email+") n'existe !")

    if request.COOKIES.get('user') is None:
        return redirect('/')

    cookie = request.COOKIES.get('user')
    cookie_dict=json.loads(cookie)
    tokentime=cookie_dict['tokentime']
    print(tokentime)
    datetime_object = time.strptime(tokentime, "%d/%m/%Y, %H:%M:%S")
    if time.strptime(datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S"),"%d/%m/%Y, %H:%M:%S")>datetime_object:
        return redirect('/')
    
    if request.GET.get('action', '')=='delete':
        x=ClientUser.objects.get(pk=user_id).delete()
        #if x>=1:
        return HttpResponse("Utilisateur "+str(user_id)+" deleted successfully")
        #else:
        #    return HttpResponseServerError("Utilisateur "+str(user_id)+" deletion failed")
    
    else:
        context={}
        if user_id!=0:
            user=ClientUser.objects.get(id=user_id)
            context=user.__dict__
            print(context)

        
        context['action']=request.GET.get('action', '')
        context['modification']=context['action']=='edit'
        
        eglises=[]
        for eglise in Eglise.objects.all():
            eglises.append(eglise.__dict__)
        context['eglises']=eglises
        
        return render(request,"forms/user.html", context=context)


#ACTION ON MODELS DATA

#view that permits to make an action on church/eglise like creation, update, deletion
@csrf_exempt
def save_eglise(request):
    if request.COOKIES.get('user') is None:
        return redirect('/')

    cookie = request.COOKIES.get('user')
    cookie_dict=json.loads(cookie)
    tokentime=cookie_dict['tokentime']
    print(tokentime)
    datetime_object = time.strptime(tokentime, "%d/%m/%Y, %H:%M:%S")
    if time.strptime(datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S"),"%d/%m/%Y, %H:%M:%S")>datetime_object:
        return redirect('/')
    
    if request.is_ajax():
        if request.method == 'POST':
            print ('Raw Data: "%s"' % request.body)
            obj=json.loads(request.body)
            eglise=Eglise()
            obj['diocese_id']=int(obj['diocese'])
            eglise.__dict__.update(obj)
            eglise.save(force_insert=True)
            created_eglise=Eglise.objects.last()
            if "creneaux" in obj:
                creneaux=obj["creneaux"].split(",")
                del obj['creneaux']
                for cr in creneaux:
                    created_eglise.creneaux.add(CreneauReligieux.objects.get(pk=int(cr)))
            created_eglise.save(force_update=True)
        elif request.method == 'PUT':
            print ('Raw Data: "%s"' % request.body)
            obj=json.loads(request.body)
            
            try:
                update=Eglise.objects.get(pk=obj['id'])
                del obj['id']
                obj['diocese_id']=int(obj['diocese'])
                if "creneaux" in obj:
                    creneaux=obj["creneaux"].split(",")
                    del obj['creneaux']
                    for cr in creneaux:
                        if CreneauReligieux.objects.get(pk=int(cr)) not in update.creneaux.all():
                            update.creneaux.add(CreneauReligieux.objects.get(pk=int(cr)))
                update.__dict__.update(obj)
                update.save(force_update=True)
            except Eglise.DoesNotExist:
                raise Http404("Impossible de mettre à jour une église non enregistrée !!!")

        elif request.method == 'DELETE':
            print ('Raw Data: "%s"' % request.body)
            obj=json.loads(request.body)
            id=request.DELETE.get('id', '')
            try:
                update=Eglise.objects.get(pk=id)
                update.delete()
            except Eglise.DoesNotExist:
                raise Http404("Impossible de supprimer une église non enregistrée !!!")
    return HttpResponse("OK")

#view that permits to make an event on user like creation, update, deletion
@csrf_exempt
def save_event(request):
    if request.COOKIES.get('user') is None:
        return redirect('/')

    cookie = request.COOKIES.get('user')
    cookie_dict=json.loads(cookie)
    tokentime=cookie_dict['tokentime']
    print(tokentime)
    datetime_object = time.strptime(tokentime, "%d/%m/%Y, %H:%M:%S")
    if time.strptime(datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S"),"%d/%m/%Y, %H:%M:%S")>datetime_object:
        return redirect('/')
    
    if request.is_ajax():
        if request.method == 'POST':
            print ('Raw Data: "%s"' % request.body)
            obj=json.loads(request.body)
            datetime_object = datetime.datetime.strptime(obj['heure'], '%H:%M')
            datetime_object1 = datetime.datetime.strptime(obj['starttime'], '%d/%m/%Y, %H:%M:%S') if 'starttime' in obj else datetime.datetime.now()
            datetime_object2 = datetime.datetime.strptime(obj['endtime'], '%d/%m/%Y, %H:%M:%S') if 'endtime' in obj else datetime.datetime.now()
            
            
            if 'pretres' in obj:
                pretres=obj["pretres"].split(",")
                del obj['pretres']
            del obj['heure']
            del obj['starttime']
            del obj['endtime']
            event=Event()
            obj['eglise_id']=int(obj['eglise'])
            event.heure=datetime_object
            event.starttime=datetime_object1
            event.endtime=datetime_object2
            event.__dict__.update(obj)
            event.save(force_insert=True)
            if 'pretres' in obj:
                created_event=Event.objects.last()
                for pr in pretres:
                    created_event.pretres.add(Pretre.objects.get(pk=int(pr)))
                # created_event.set_time(datetime_object)
                created_event.save(force_update=True)
        elif request.method == 'PUT':
            print ('Raw Data: "%s"' % request.body)
            obj=json.loads(request.body)
            datetime_object = datetime.datetime.strptime(obj['heure'], '%H:%M')
            datetime_object1 = datetime.datetime.strptime(obj['starttime'], '%d/%m/%Y, %H:%M:%S') if 'starttime' in obj else datetime.datetime.now()
            datetime_object2 = datetime.datetime.strptime(obj['endtime'], '%d/%m/%Y, %H:%M:%S') if 'endtime' in obj else datetime.datetime.now()
            
            
            try:
                update=Event.objects.get(pk=obj['id'])
                del obj['id']
                if 'pretres' in obj:
                    pretres=obj["pretres"].split(",")
                    del obj['pretres']
                del obj['heure']
                del obj['starttime']
                del obj['endtime']
                
                obj['eglise_id']=int(obj['eglise'])
                update.__dict__.update(obj)
                if 'pretres' in obj:
                    for pr in pretres:
                        if Pretre.objects.get(pk=int(pr)) not in update.pretres.all():
                            update.pretres.add(Pretre.objects.get(pk=int(pr)))
                update.heure=datetime_object
                update.starttime=datetime_object1
                update.endtime=datetime_object2
                update.save(force_update=True)
            except Event.DoesNotExist:
                raise Http404("Impossible de mettre à jour un événement non enregistré !!!")

        elif request.method == 'DELETE':
            print ('Raw Data: "%s"' % request.body)
            obj=json.loads(request.body)
            id=request.DELETE.get('id', '')
            try:
                update=Event.objects.get(pk=id)
                update.delete()
            except Event.DoesNotExist:
                raise Http404("Impossible de supprimer un événement non enregistré !!!")
    return HttpResponse("OK")

#view that permits to make an action on creneau like creation, update, deletion
@csrf_exempt
def save_creneau(request):
    if request.COOKIES.get('user') is None:
        return redirect('/')

    cookie = request.COOKIES.get('user')
    cookie_dict=json.loads(cookie)
    tokentime=cookie_dict['tokentime']
    print(tokentime)
    datetime_object = time.strptime(tokentime, "%d/%m/%Y, %H:%M:%S")
    if time.strptime(datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S"),"%d/%m/%Y, %H:%M:%S")>datetime_object:
        return redirect('/')
    
    if request.is_ajax():
        if request.method == 'POST':
            print ('Raw Data: "%s"' % request.body)
            obj=json.loads(request.body)
            datetime_object = datetime.datetime.strptime(obj['heure'], '%H:%M')
            datetime_object1 = datetime.datetime.strptime(obj['starttime'], '%d/%m/%Y, %H:%M:%S') if 'starttime' in obj else datetime.datetime.now()
            datetime_object2 = datetime.datetime.strptime(obj['endtime'], '%d/%m/%Y, %H:%M:%S') if 'endtime' in obj else datetime.datetime.now()
            creneau=CreneauReligieux(jour=obj['jour'], frequence=obj['frequence'])
            creneau.heure=datetime_object
            creneau.starttime=datetime_object1
            creneau.endtime=datetime_object2
            creneau.save(force_insert=True)
        elif request.method == 'PUT':
            print ('Raw Data: "%s"' % request.body)
            obj=json.loads(request.body)
            
            try:
                update=CreneauReligieux.objects.get(pk=obj['id'])
                datetime_object = datetime.datetime.strptime(obj['heure'], '%H:%M')
                datetime_object1 = datetime.datetime.strptime(obj['starttime'], '%d/%m/%Y, %H:%M:%S') if 'starttime' in obj else datetime.datetime.now()
                datetime_object2 = datetime.datetime.strptime(obj['endtime'], '%d/%m/%Y, %H:%M:%S') if 'endtime' in obj else datetime.datetime.now()
            
                del obj['id']
                del obj['heure']
                if 'startime' in obj:
                    del obj['starttime']
                if 'endtime' in obj:
                    del obj['endtime']
                # update.__dict__.update(obj)
                update.jour=obj['jour']
                update.starttime=datetime_object1
                update.endtime=datetime_object2
                update.frequence=obj['frequence']
                update.heure=datetime_object
                update.save(force_update=True)
            except CreneauReligieux.DoesNotExist:
                raise Http404("Impossible de mettre à jour un créneau non enregistré !!!")

        elif request.method == 'DELETE':
            print ('Raw Data: "%s"' % request.body)
            obj=json.loads(request.body)
            id=request.DELETE.get('id', '')
            try:
                update=CreneauReligieux.objects.get(pk=id)
                update.delete()
            except CreneauReligieux.DoesNotExist:
                raise Http404("Impossible de supprimer un créneau non enregistré !!!")
    return HttpResponse("OK")

#view that permits to make an action on diocese like creation, update, deletion
@csrf_exempt
def save_diocese(request):
    if request.COOKIES.get('user') is None:
        return redirect('/')

    cookie = request.COOKIES.get('user')
    cookie_dict=json.loads(cookie)
    tokentime=cookie_dict['tokentime']
    print(tokentime)
    datetime_object = time.strptime(tokentime, "%d/%m/%Y, %H:%M:%S")
    if time.strptime(datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S"),"%d/%m/%Y, %H:%M:%S")>datetime_object:
        return redirect('/')
    
    if request.is_ajax():
        if request.method == 'POST':
            print ('Raw Data: "%s"' % request.body)
            obj=json.loads(request.body)
            diocese=Diocese(denomination=obj['denomination'], archidiocese=Archidiocese.objects.get(pk=int(obj['archidiocese'])), Communes=[obj['denomination'].split(" ")[-1].lower()]).save(force_insert=True)
        elif request.method == 'PUT':
            print ('Raw Data: "%s"' % request.body)
            obj=json.loads(request.body)
            
            try:
                update=Diocese.objects.get(pk=obj['id'])
                del obj['id']
                obj['archidiocese_id']=int(obj['archidiocese'])
                update.__dict__.update(obj)
                update.Communes=[obj['denomination'].split(" ")[-1].lower()]
                update.save(force_update=True)
            except Diocese.DoesNotExist:
                raise Http404("Impossible de mettre à jour un diocese non enregistré !!!")

        elif request.method == 'DELETE':
            print ('Raw Data: "%s"' % request.body)
            obj=json.loads(request.body)
            id=request.DELETE.get('id', '')
            try:
                update=Diocese.objects.get(pk=id)
                update.delete()
            except Diocese.DoesNotExist:
                raise Http404("Impossible de supprimer un diocèse non enregistré !!!")
    return HttpResponse("OK")

#view that permits to make an action on demande(messe) like creation, update, deletion
@csrf_exempt
def save_demande(request):
    obj=json.loads(request.body)
    if not obj.get('isMobile'):
        print("Browser query")
        if request.COOKIES.get('user') is None:
            return redirect('/')

        cookie = request.COOKIES.get('user')
        cookie_dict=json.loads(cookie)
        tokentime=cookie_dict['tokentime']
        print(tokentime)
        datetime_object = time.strptime(tokentime, "%d/%m/%Y, %H:%M:%S")
        if time.strptime(datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S"),"%d/%m/%Y, %H:%M:%S")>datetime_object:
            return redirect('/')
    
    else:
        print("Mobile query")


    if request.method == 'POST':
        print ('Raw Data: "%s"' % request.body)
        obj=json.loads(request.body)
        demande=DemandeMesse()
        obj['event_id']=int(obj['messe'])
        obj['etat'] = str2bool(obj['etat'])
        del obj['messe']
        demande.__dict__.update(obj)
        demande.save(force_insert=True)
        if obj.get('isMobile'):
            json_r=demande.__dict__
            json_r['messe']=demande.event.__dict__
            json_r['id']=DemandeMesse.objects.latest().pk
            return JsonResponse(json_r, safe=False, json_dumps_params={'ensure_ascii': False})
    elif request.method == 'PUT':
        print ('Raw Data: "%s"' % request.body)
        obj=json.loads(request.body)
        
        try:
            update=DemandeMesse.objects.get(pk=obj['id'])
            del obj['id']
            obj['event_id']=int(obj['messe'])
            obj['etat'] = str2bool(obj['etat'])
            del obj['messe']
            update.__dict__.update(obj)
            update.save(force_update=True)
        except DemandeMesse.DoesNotExist:
            raise Http404("Impossible de mettre à jour une demande de messe non enregistrée !!!")

    elif request.method == 'DELETE':
        print ('Raw Data: "%s"' % request.body)
        obj=json.loads(request.body)
        id=request.DELETE.get('id', '')
        try:
            update=DemandeMesse.objects.get(pk=id)
            update.delete()
        except DemandeMesse.DoesNotExist:
            raise Http404("Impossible de supprimer une demande de messe non enregistrée !!!")
    return HttpResponse("OK")

#view that permits to make an action on archidiocese like creation, update, deletion
@csrf_exempt
def save_archi(request):
    if request.COOKIES.get('user') is None:
        return redirect('/')

    cookie = request.COOKIES.get('user')
    cookie_dict=json.loads(cookie)
    tokentime=cookie_dict['tokentime']
    print(tokentime)
    datetime_object = time.strptime(tokentime, "%d/%m/%Y, %H:%M:%S")
    if time.strptime(datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S"),"%d/%m/%Y, %H:%M:%S")>datetime_object:
        return redirect('/')
    
    if request.is_ajax():
        if request.method == 'POST':
            print ('Raw Data: "%s"' % request.body)
            obj=json.loads(request.body)
            Archidiocese(denomination=obj['denomination']).save(force_insert=True)
        elif request.method == 'PUT':
            print ('Raw Data: "%s"' % request.body)
            obj=json.loads(request.body)
            
            try:
                update=Archidiocese.objects.get(pk=obj['id'])
                del obj['id']
                update.__dict__.update(obj)
                update.save(force_update=True)
            except Archidiocese.DoesNotExist:
                raise Http404("Impossible de mettre à jour un archidiocese non enregistré !!!")

        elif request.method == 'DELETE':
            print ('Raw Data: "%s"' % request.body)
            obj=json.loads(request.body)
            id=request.DELETE.get('id', '')
            try:
                update=Archidiocese.objects.get(pk=id)
                update.delete()
            except Archidiocese.DoesNotExist:
                raise Http404("Impossible de supprimer un archidiocèse non enregistré !!!")
    return HttpResponse("OK")

#view that permits to make an action on user like creation, update, deletion
@csrf_exempt
def save_user(request):
    if request.COOKIES.get('user') is None:
        return redirect('/')

    cookie = request.COOKIES.get('user')
    cookie_dict=json.loads(cookie)
    tokentime=cookie_dict['tokentime']
    print(tokentime)
    datetime_object = time.strptime(tokentime, "%d/%m/%Y, %H:%M:%S")
    if time.strptime(datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S"),"%d/%m/%Y, %H:%M:%S")>datetime_object:
        return redirect('/')
    
    if request.is_ajax():
        if request.method == 'POST':
            print ('Raw Data: "%s"' % request.body)
            obj=json.loads(request.body)
            user=ClientUser(type=obj['type'], eglise=int(obj['eglise']), username=obj['username'], firstname=obj['firstname'], lastname=obj['lastname'], password=get_random_string(), token=csrf._get_new_csrf_token()).save(force_insert=True)
        elif request.method == 'PUT':
            print ('Raw Data: "%s"' % request.body)
            obj=json.loads(request.body)            
            try:
                update=ClientUser.objects.get(pk=obj['id'])
                del obj['id']
                obj['eglise_id']=int(obj['eglise'])
                update.__dict__.update(obj)
                update.save(force_update=True)
            except ClientUser.DoesNotExist:
                raise Http404("Impossible de mettre à jour un utilisateur non enregistré !!!")

        elif request.method == 'DELETE':
            print ('Raw Data: "%s"' % request.body)
            obj=json.loads(request.body)
            id=request.DELETE.get('id', '')
            try:
                update=ClientUser.objects.get(pk=id)
                update.delete()
            except ClientUser.DoesNotExist:
                raise Http404("Impossible de supprimer un utilisateur non enregistré !!!")
    return HttpResponse("OK")

#view that permits to store new password for user after renew 
@csrf_exempt
def new_password(request):
    if request.is_ajax():
        if request.method == 'PUT':
            print ('Raw Data: "%s"' % request.body)
            obj=json.loads(request.body)            
            try:
                update=ClientUser.objects.get(pk=obj['id'])
                del obj['repassword']
                # del obj['show']
                update.__dict__.update(obj)
                update.save(force_update=True)
            except ClientUser.DoesNotExist:
                raise Http404("Impossible de mettre à jour un utilisateur non enregistré !!!")

        else:
            return HttpResponseServerError("Cette requête n'est pas autorisée !!!")
    return HttpResponse("OK")

#view that returns dynamically data according to user query and his profile role
def update_table(request):
    data=[]
    Header=[]
    usertype=request.GET.get('usertype', '')
    eglisename=request.GET.get('eglise', '')
    if usertype!="superadmin":
        eglise_id=Eglise.objects.get(denomination=eglisename).pk
    tablename=request.GET.get('modelname', '')
    if tablename=="clientuser":
        title="Utilisateurs"
        header = [field.name for field in ClientUser._meta.get_fields()]
        header.remove("password")
        header.remove("token")
        header.remove("tokentime")
        if usertype!="superadmin":
            data=[
                [getattr(instance, field) for field in header ]
                for instance in ClientUser.objects.filter(eglise=eglise_id).all()
            ]
        else:
            data=[
                [getattr(instance, field) for field in header ]
                for instance in ClientUser.objects.all()
            ]
    elif tablename=="event":
        title="Evènements"
        header = ["id", "denomination", "type", "eglise", "temps", "pretres"]
        # header.remove('demandemesse')
        # header.remove('pretre')
        data=[]
        if usertype!="superadmin":
            for Ev in Event.objects.filter(eglise=eglise_id).all():
                inst=[]
                inst.append(getattr(Ev, 'id'))
                inst.append(getattr(Ev, 'denomination'))
                inst.append(Ev.get_type_display())
                inst.append(getattr(Ev, 'eglise'))
                inst.append(Ev.get_time())
                pretres=Ev.pretres.all()
                data_pr=[
                    instance.nom+" "+instance.prenoms for instance in pretres
                ]
                arr="\n ".join(data_pr)
                inst.append(arr)
                data.append(inst)
        else:
            for Ev in Event.objects.all():
                inst=[]
                inst.append(getattr(Ev, 'id'))
                inst.append(getattr(Ev, 'denomination'))
                inst.append(Ev.get_type_display())
                inst.append(getattr(Ev, 'eglise'))
                inst.append(Ev.get_time())
                pretres=Ev.pretres.all()
                data_pr=[
                    instance.nom+" "+instance.prenoms for instance in pretres
                ]
                arr="\n ".join(data_pr)
                inst.append(arr)
                data.append(inst)
        # data=[
        #         [getattr(instance, field) if not 'None' in getattr(instance, field) else [getattr(instance, field)] for field in header ]
        #         for instance in Event.objects.all()
        #     ]
    elif tablename=="eglise":
        title="Eglises"
        header = [field.name for field in Eglise._meta.get_fields()]
        header.remove('pretre')
        header.remove('event')
        header.remove('clientuser')
        data=[]
        for Eg in Eglise.objects.all():
            inst=[]
            inst.append(getattr(Eg, 'id'))
            inst.append(getattr(Eg, 'denomination'))
            inst.append(getattr(Eg, 'addresse'))
            inst.append(Eg.get_type_display())
            inst.append(getattr(Eg, 'diocese'))
            inst.append(getattr(Eg, 'commune'))
            inst.append(getattr(Eg, 'tel'))
            inst.append(getattr(Eg, 'cel'))
            inst.append(getattr(Eg, 'email'))
            inst.append(getattr(Eg, 'web'))
            inst.append(getattr(Eg, 'facebook'))
            inst.append(getattr(Eg, 'youtube'))
            inst.append(getattr(Eg, 'latitude'))
            inst.append(getattr(Eg, 'longitude'))
            creneaux=Eg.creneaux.all()
            data_pr=[]
            for instance in creneaux:
                if instance.frequence == 'Pas_de_frequence':
                    data_pr.append(instance.jour+" à "+instance.heure.strftime("%H:%M"))
                elif instance.frequence == 'Quotidienne':
                    data_pr.append("Tous les jours à "+instance.heure.strftime("%H:%M"))
                else :
                    data_pr.append("Tous les "+instance.jour+"s à "+instance.heure.strftime("%H:%M"))

            arr="\n ".join(data_pr)
            inst.append(arr)
            data.append(inst)
        # data=[
        #         [getattr(instance, field) for field in header ]
        #         for instance in Eglise.objects.all()
        #     ]
    elif tablename=="diocese":
        title = "Diocèses"
        header = [field.name for field in Diocese._meta.get_fields()]
        header.remove('eglise')
        data=[
                [getattr(instance, field) for field in header ]
                for instance in Diocese.objects.all()
            ]

    elif tablename=="demande":
        title = "Demandes"
        data=[]
        header = [field.name for field in DemandeMesse._meta.get_fields()]
        if usertype!="superadmin":
            queryset=DemandeMesse.objects.all()
            demandes=[]
            for demande in queryset:
                if demande.event.eglise==eglise_id:
                    demandes.append(demande)

            data=[
                    [getattr(instance, field) for field in header ]
                    for instance in demandes
                ]
        else:
            data=[
                [getattr(instance, field) for field in header ]
                for instance in DemandeMesse.objects.all()
            ]
    elif tablename=="archidiocese":
        title="Archidiocèses"
        header = [field.name for field in Archidiocese._meta.get_fields()]
        header.remove('diocese')
        data=[
                [getattr(instance, field) for field in header ]
                for instance in Archidiocese.objects.all()
            ]
    elif tablename=="pretre":
        title="Pretres"
        header = [field.name for field in Pretre._meta.get_fields()]
        header.remove('event')
        if usertype!="superadmin":
            data=[
                    [getattr(instance, field) for field in header ]
                    for instance in Pretre.objects.filter(eglise=eglise_id).all()
                ]
        else:
            data=[
                    [getattr(instance, field) for field in header ]
                    for instance in Pretre.objects.all()
                ]

    elif tablename=='accueil':
        if usertype!="superadmin":
            now = datetime.datetime.now()
            # all_eglises=Eglise.objects.all()
            all_dm=DemandeMesse.objects.all()
            all_ev=Event.objects.filter(eglise=Eglise.objects.get(denomination=eglisename)).order_by('-starttime')[:5]
            # nb_par=all_eglise.count()
            nb_dm=all_dm.count()
            active_events=Event.objects.filter(endtime__gte=now, eglise=Eglise.objects.get(denomination=eglisename)).order_by('-starttime')[:5]
            nb_dm_active=0
            for ev in active_events:
                nb_dm_active+=DemandeMesse.objects.filter(event=ev).count()
            nb_dm_paid=DemandeMesse.objects.filter(etat=True).count()
            nb_dm_unpaid=DemandeMesse.objects.filter(etat=False).count()
            data=[]
            header = ['Date et Heure', 'Nombre de messes', 'Montant total', 'Montant à rembourser']
            for ev in all_ev:
                dateh=ev.get_time()
                nb_dm=0
                mt_total=0
                mt_reste=0
                for dm in all_dm:
                    if dm.event.eglise.denomination == eglisename:
                        nb_dm+=1
                        mt_total+=3000
                        if not dm.etat :
                            mt_reste+=3000
                r=[dateh, nb_dm, nb_dm*3000, mt_reste]
                data.append(r)
            print(header)
            print(data)
            resp={'usertype':usertype, 'data':data, 'header': header, 'nb_dm':nb_dm, 'nb_dm_active':nb_dm_active, 'dm_paid':nb_dm_paid*3000, 'dm_unpaid':nb_dm_unpaid*3000, 'key':tablename}
            print(resp)
            return render(request, 'modeltable/modeltable.html', resp)
        else:
            now = datetime.datetime.now()
            all_eglises=Eglise.objects.all()
            all_dm=DemandeMesse.objects.all()
            all_ev=Event.objects.all()
            nb_par=all_eglises.count()
            nb_dm=all_dm.count()
            active_events=Event.objects.filter(endtime__gte=now).order_by('-starttime')[:5]
            nb_dm_active=0
            for ev in active_events:
                nb_dm_active+=DemandeMesse.objects.filter(event=ev).count()
            nb_dm_paid=DemandeMesse.objects.filter(etat=True).count()
            nb_dm_unpaid=DemandeMesse.objects.filter(etat=False).count()
            data=[]
            header = ['Paroisse', 'Nombre de messes', 'Montant total', 'Montant à rembourser']
            for eg in all_eglises:
                nb_messe=0
                mt_total=0
                mt_reste=0
                for ev in all_ev:
                    if ev.eglise.denomination == eg.denomination:
                        nb_messe+=1
                
                for dm in all_dm:
                    if dm.event.eglise.denomination == eg.denomination:
                        mt_total+=3000
                        if not dm.etat:
                            mt_reste+=3000
                r=[eg.denomination, nb_messe, mt_total, mt_reste]
                data.append(r)
        
            print(header)
            print(data)
            resp={'usertype':usertype, 'data':data, 'header': header,'nb_par':nb_par, 'nb_dm':nb_dm, 'nb_dm_active':nb_dm_active, 'dm_paid':nb_dm_paid*3000, 'dm_unpaid':nb_dm_unpaid*3000, 'key':tablename}
            print(resp)
            return render(request, 'modeltable/modeltable.html', resp)
    else:
        title="Recherches"
        header = [field.name for field in Seeked._meta.get_fields()]
        data=[
                [getattr(instance, field) for field in header ]
                for instance in Seeked.objects.all()
            ]
    
    print(header)
    print(data)
    resp={'data':data, 'header': header, 'title':title, 'key':tablename}
    print(resp)
    # return JsonResponse(json.dumps(resp), safe=False, json_dumps_params={'ensure_ascii': False})
    return render(request, 'modeltable/modeltable.html', resp)


def get_events(request):
    data=[]
    now = datetime.datetime.now()

    for Ev in Event.objects.filter(starttime__gte=now).order_by('-starttime')[:5]:
        inst={}
        inst['id']=getattr(Ev, 'id')
        inst['denomination']=getattr(Ev, 'denomination')
        inst['type']=Ev.get_type_display()
        inst['eglise']=Ev.eglise.denomination
        inst['time']=Ev.get_time()
        data.append(inst)
    
    return JsonResponse(data, safe=False, json_dumps_params={'ensure_ascii': False})

#function that gets representation of callable function
def get_repr(value): 
    if callable(value):
        return '%s' % value
    return value

import random
import string
#function that generates random string for password when creating new user
def get_random_string(length=12):
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    print("Random string of length", length, "is:", result_str)

def str2bool(v): # Note the self to allow it to be in a class
  return v.lower() in ('yes', 'true', 't', '1', 'yea', 'verily')