# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Archidiocese, Eglise, Event, Diocese, Pretre, CreneauReligieux, Seeked, ClientUser, DemandeMesse

admin.site.register(Archidiocese)
admin.site.register(Diocese)
admin.site.register(Eglise)
admin.site.register(Pretre)
admin.site.register(Event)
admin.site.register(CreneauReligieux)
admin.site.register(Seeked)
admin.site.register(ClientUser)
admin.site.register(DemandeMesse)
