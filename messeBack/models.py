# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from datetime import datetime, timedelta
import time
#from django.contrib.gis.geos import Point
#from location_field.models.spatial import LocationField
from django.contrib.postgres.fields.array import ArrayField
from django.utils.timezone import now

EGLISE_CHOICE=[("Cat", "Cathédrale"), ("Chap","Chapelle"), ("Simple","Simple"), ("Paroisse","Simple")]
USER_CHOICE=[("Superadmin", "superadmin"), ("Admin", "admin"), ("Secre","Secretaire")]
ETAT_PAID=[("P", "payé"), ("I", "Impayé")]
DAY_CHOICE=[("Lundi", "Lundi"), ("Mardi","Mardi"), ("Mercredi","Mercredi"), ("Jeudi","Jeudi"), ("Vendredi","Vendredi"), ("Samedi","Samedi"), ("Dimanche","Dimanche")]
PRETRE_CHOICE=[("Card","Cardinal"), ("Arch","Archévèque"), ("Ev","Evèque"), ("Cu","Curé"), ("Diac","Diacre"), ("Simple","Simple")]
EVENT_CHOICE=[("Me","Messe"), ("Mefi","Messe filmée"), ("Ado","Adoration"), ("Conf","Confession"), ("Pel","Pélérinage"), ("Bapt","Baptême"), ("Mari","Mariage"), ("Veil","Veillée")]
CRENEAU_FREQUENCE_CHOICE=[("Quotidienne","Quotidienne"), ("Hebdomadaire","Hebdomadaire"), ("Pas_de_frequence","Pas_de_frequence")]
CRENEAU_CHOICE=[("Messe","Messe"), ("Confession","Confession")]

class CustomDateTimeField(models.DateTimeField):
    def value_to_string(self, obj):
        val = self.value_from_object(obj)
        if val:
            val.replace(microsecond=0)
            return val.isoformat()
        return ''

class GetNestedFieldsMixin(object):
    link_fields = set((u"ForeignKey", u"OneToOneField"))

    @classmethod
    def _get_nested_fields(cls, obj):
        for field in obj._meta.fields:
            value = field.value_from_object(obj)
            is_link = field.get_internal_type() in cls.link_fields
            if is_link:
                nested_obj = field.rel.to.objects.get(pk=value)
                for name, field, value in  cls._get_nested_fields(nested_obj):
                    yield name, field, value
            else:
                yield obj._meta.object_name, field.name, \
                    field.value_to_string(obj)

    def get_nested_fields(self):
        return self._get_nested_fields(self)

class CreneauReligieux(GetNestedFieldsMixin,models.Model):
    jour = models.CharField(max_length=200, default="Dimanche")

    type = models.CharField(max_length=200, choices=CRENEAU_CHOICE, default="Messe")

    heure = models.TimeField()

    frequence = models.CharField(max_length=200, choices=CRENEAU_FREQUENCE_CHOICE, default="Pas_de_frequence")

    starttime = CustomDateTimeField(default=datetime.now())
    
    
    endtime = CustomDateTimeField(default=datetime.now()+timedelta(days = 365))
    
    def set_starttime(self, datetime: datetime):
        self.time = datetime

    def set_endtime(self, datetime: datetime):
        self.time = datetime


    def __str__(self):
        if self.frequence=='Pas_de_frequence':
            jour_arr=self.jour.split(',')
            return ", ".join(jour_arr)+ " -> " +self.heure.strftime("%H:%M")
        elif self.frequence=='Quotidienne':
            return "Tous les jours -> " + self.heure.strftime("%H:%M")
        else:
            jour_arr=self.jour.split(',')
            return "s, ".join(jour_arr)+ "s -> "+self.heure.strftime("%H:%M")

class Archidiocese(models.Model):
    denomination = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return self.denomination
    
class Diocese(GetNestedFieldsMixin, models.Model):
    denomination = models.CharField(max_length=200)
    Communes = ArrayField(
            models.CharField(max_length=100, default="Abidjan"),
            size=100
        )
    archidiocese = models.ForeignKey(Archidiocese, on_delete=models.CASCADE, blank=True)

    def __str__(self):
        return self.denomination

"""     def __getattr__(self, name):
        print(name)
        if self.inherit and hasattr(self.inherit, name):
            return getattr(self.inherit, name, None)
        elif hasattr(self, '_'+name):
            return getattr(self, '_'+name, None)
        return super(Diocese, self).__getattr__(name)
 """
class Seeked(GetNestedFieldsMixin, models.Model):
    value = models.CharField(max_length=200)
    iteration = models.IntegerField(default=1)

    def __str__(self):
        return self.value

class Eglise(GetNestedFieldsMixin, models.Model):
    denomination = models.CharField(max_length=200)
    addresse = models.CharField(max_length=200)
    type = models.CharField(max_length=200, choices=EGLISE_CHOICE, default="Simple")
    diocese = models.ForeignKey(Diocese, on_delete=models.CASCADE)
    commune = models.CharField(max_length=200, default="Cocody")
    tel = models.CharField(max_length=10, default="")
    cel = models.CharField(max_length=10, default="")
    email = models.CharField(max_length=200, default="")
    web = models.CharField(max_length=200, default="to_define")
    facebook = models.CharField(max_length=200, default="to_define")
    youtube = models.CharField(max_length=200, default="to_define")
    latitude = models.FloatField(default=5.370462)
    longitude = models.FloatField(default=-3.9980495)
    creneaux = models.ManyToManyField(CreneauReligieux)

    def __str__(self):
        return self.denomination

"""     def __getattr__(self, name):
        print(name)
        if self.inherit and hasattr(self.inherit, name):
            return getattr(self.inherit, name, None)
        elif hasattr(self, '_'+name):
            return getattr(self, '_'+name, None)
        return super(Eglise, self).__getattr__(name)
 """
class Pretre(GetNestedFieldsMixin, models.Model):
    nom = models.CharField(max_length=200)
    prenoms = models.CharField(max_length=200)
    titre = models.CharField(max_length=200, choices=PRETRE_CHOICE, default=6)
    eglise = models.ForeignKey(Eglise, on_delete=models.CASCADE)

    def __str__(self):
        return "Père "+self.nom + " " + self.prenoms
    
"""     def __getattr__(self, name):
        print(name)
        if self.inherit and hasattr(self.inherit, name):
            return getattr(self.inherit, name, None)
        elif hasattr(self, '_'+name):
            return getattr(self, '_'+name, None)
        return super(Pretre, self).__getattr__(name)

 """
class Event(GetNestedFieldsMixin, models.Model):
    denomination = models.CharField(max_length=200)
    type = models.CharField(max_length=200, choices=EVENT_CHOICE, default="Me")
    eglise = models.ForeignKey(Eglise, on_delete=models.CASCADE)
    pretres = models.ManyToManyField(Pretre)
    #localisation = LocationField(based_fields=['city'], zoom=10, default=Point(5.370462, -3.9980495))
    jour = models.CharField(max_length=200, default="Dimanche")

    # type = models.CharField(max_length=200, choices=CRENEAU_CHOICE, default="Dimanche")

    heure = models.TimeField()

    frequence = models.CharField(max_length=200, choices=CRENEAU_FREQUENCE_CHOICE, default="Pas_de_frequence")

    starttime = CustomDateTimeField(default=datetime.now())
    
    
    endtime = CustomDateTimeField(default=datetime.now()+timedelta(days = 365))

    def set_starttime(self, datetime: datetime):
        self.time = datetime

    def set_endtime(self, datetime: datetime):
        self.time = datetime

    def get_time(self):
        if self.frequence=='Pas_de_frequence':
            jour_arr=self.jour.split(', ')
            return ", ".join(jour_arr)+ " -> " +self.heure.strftime("%H:%M")
        elif self.frequence=='Quotidienne':
            return "Tous les jours -> " + self.heure.strftime("%H:%M")
        else:
            jour_arr=self.jour.split(', ')
            return ", ".join(jour_arr)+ " -> "+self.heure.strftime("%H:%M")

    def __str__(self):
        if self.frequence=='Pas_de_frequence':
            jour_arr=self.jour.split(', ')
            return ", ".join(jour_arr)+ " -> " +self.heure.strftime("%H:%M")+" à "+self.eglise.denomination
        elif self.frequence=='Quotidienne':
            return "Tous les jours -> " + self.heure.strftime("%H:%M")+" à "+self.eglise.denomination
        else:
            jour_arr=self.jour.split(', ')
            return "s, ".join(jour_arr)+ "s -> "+self.heure.strftime("%H:%M")+" à "+self.eglise.denomination

    
"""     def __getattr__(self, name):
        print(name)
        if self.inherit and hasattr(self.inherit, name):
            return getattr(self.inherit, name, None)
        elif hasattr(self, '_'+name):
            return getattr(self, '_'+name, None)
        return super(Event, self).__getattr__(name)
 """
class ClientUser(GetNestedFieldsMixin, models.Model):
    username = models.CharField(max_length=200)
    firstname = models.CharField(max_length=200, blank=True)
    lastname = models.CharField(max_length=200, blank=True)
    password = models.CharField(max_length=200)
    email = models.CharField(max_length=200)
    type = models.CharField(max_length=200, choices=USER_CHOICE, default="Secre")
    eglise = models.ForeignKey(Eglise, on_delete=models.CASCADE)
    token = models.CharField(max_length=200, blank=True)
    tokentime = models.CharField(default=datetime.now().strftime("%d/%m/%Y, %H:%M:%S"), max_length=200)

    def __str__(self):
        return self.firstname+" "+self.lastname

    def isAuthenticated(self):
        return time.strptime(datetime.now().strftime("%d/%m/%Y, %H:%M:%S"),"%d/%m/%Y, %H:%M:%S") <time.strptime(self.tokentime, "%d/%m/%Y, %H:%M:%S")

    """ def __getattr__(self, name):
        print(name)
        if self.inherit and hasattr(self.inherit, name):
            return getattr(self.inherit, name, None)
        elif hasattr(self, '_'+name):
            return getattr(self, '_'+name, None)
        return super(ClientUser, self).__getattr__(name)
 """

class DemandeMesse(GetNestedFieldsMixin, models.Model):
    name = models.CharField(max_length=200, blank=True, default="Un paroissien")
    objet = models.CharField(max_length=200, blank=True)
    description = models.TextField(max_length=500)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    etat = models.BooleanField(default=False)

    def __str__(self):
        state="payé" if self.etat==True else 'impayé' 
        return self.description[:50]+"... "+ state

